package main;

import domain.Comentario;

public class Prueba {

    public static void main(String[] args) {
        Comentario comment = new Comentario();
        comment.escribirComentario("/this is a comment*/");

        Comentario comment2 = new Comentario();
        comment2.escribirComentario("/*prueba2*");

        Comentario comment3 = new Comentario();
        comment3.escribirComentario("/*comentario escrito correctamente*/");
    }
}
