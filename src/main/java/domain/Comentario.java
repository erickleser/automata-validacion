package domain;

public class Comentario {

    protected String comentario;
    private static int contador;

    public Comentario() {
        contador+=1;
    }

    public void escribirComentario(String comentario) {
      
        this.comentario = comentario;

        if (comentario.charAt(0) == '/') //verificamos que el comentario inicie en diagonal
        {
            if (comentario.charAt(1) == '*') //si tiene la diagonal procedemos a verificar el asterisco
            {
                if (comentario.charAt(comentario.length()-1) == '/') //si cumple los anteriores dos. revizamos que el ultimo caracter sea una diagonal de cierre
                {
                    if (comentario.charAt(comentario.length()-2) == '*') //si la diagonal de cierre se encuentra, verificamos el asterico precedente 
                    {
                        System.out.println("El comentario " +contador +" es correcto ");
                    }
                    else                                                        //Si no hay asterisco pero si esta la diagonal de cierre, decimos al usuario que falta el asterisco
                    {
                        System.out.println("Se esperaba un '*' para cerrar correctamente el comentario" +contador);
                    }
                }
                else if (comentario.charAt(comentario.length()-1) == '*') //si no esta la diagonal pero si el * entonces falta el /
                {
                    System.out.println("Se esperaba un '/' para cerrar correctamente el comentario " +contador);
                }
                else
                {                                                           //si no hay ninguno de los dos. notificamos que falta el cierre
                    System.out.println("Se esperaba un '*/' para cerrar correctamente el comentario " +contador);
                }
            } 
            else                        //si tiene la diagonal de inicio pero no el asterico de inicio, avisamos.
            {
                System.out.println("Se esperaba un '*' para iniciar correctamente el comentario " +contador);
            }
        } 
        else //Si no hay diagonal de inicio en el comentario. alertamos
        {
            System.out.println("Se esperaba un '/' para iniciar correctamente el comentario " +contador);
        }

    }

}
